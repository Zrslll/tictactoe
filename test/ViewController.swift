

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var gameBut: UIButton!
    
    @IBOutlet weak var aboutBut: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        gameBut.center.x = view.center.x
        gameBut.center.y = view.center.y - 100
        aboutBut.center.x = view.center.x
        aboutBut.center.y = view.center.y
       
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(RandomColor) , userInfo: nil, repeats: true)
       
        gameBut.layer.cornerRadius = 15
        gameBut.layer.borderWidth = 2
        gameBut.layer.borderColor = UIColor.blue.cgColor
     
        aboutBut.layer.cornerRadius = 15
        aboutBut.layer.borderColor = UIColor.blue.cgColor
        aboutBut.layer.borderWidth = 2
     
        
      
        RandomColor()
    }

    @objc public func RandomColor() {
    
        let red   = CGFloat((arc4random() % 256)) / 255.0
        let green = CGFloat((arc4random() % 256)) / 255.0
        let blue  = CGFloat((arc4random() % 256)) / 255.0
        let alpha = CGFloat(1.0)
  
        UIView.animate(withDuration: 4, delay: 0, options:[.autoreverse, .allowUserInteraction], animations: {
            self.view.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        }, completion:nil)
    }
    
        
    
}

