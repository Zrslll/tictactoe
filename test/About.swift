

import Foundation
import UIKit

class About: UIViewController {


    @IBOutlet weak var iconImg: UIImageView!    
    @IBOutlet weak var aboutText: UITextView!
    @IBOutlet weak var closeAbout: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       iconImg.center.x = view.center.x
       iconImg.center.y = 200
     
       iconImg.layer.cornerRadius = 15
       iconImg.layer.borderWidth = 2
       iconImg.layer.borderColor = UIColor.black.cgColor
       
        
       aboutText.center.x = view.center.x
       aboutText.center.y = view.center.y + 135
        
       aboutText.layer.cornerRadius = 15
       aboutText.layer.borderWidth = 2
       aboutText.layer.borderColor = UIColor.black.cgColor
        
       closeAbout.center.x = view.center.x
       closeAbout.center.y = aboutText.center.y + 150
       closeAbout.layer.cornerRadius = 15
       closeAbout.layer.borderWidth = 2
       closeAbout.layer.borderColor = UIColor.blue.cgColor
        
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(RandomColor) , userInfo: nil, repeats: true)
        
       RandomColor()
    }

    @objc public func RandomColor() {
         
             let red   = CGFloat((arc4random() % 256)) / 255.0
             let green = CGFloat((arc4random() % 256)) / 255.0
             let blue  = CGFloat((arc4random() % 256)) / 255.0
             let alpha = CGFloat(1.0)
       
         UIView.animate(withDuration: 4, delay: 0, options:[ .autoreverse, .allowUserInteraction], animations: {
                 self.view.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)
             }, completion:nil)
         }
}
