

import Foundation
import UIKit

class Game: UIViewController {

    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var but1: UIButton!
    @IBOutlet weak var but2: UIButton!
    @IBOutlet weak var but3: UIButton!
    @IBOutlet weak var but4: UIButton!
    @IBOutlet weak var but5: UIButton!
    @IBOutlet weak var but6: UIButton!
    @IBOutlet weak var but7: UIButton!
    @IBOutlet weak var but8: UIButton!
    @IBOutlet weak var but9: UIButton!
    @IBOutlet weak var exitBut: UIButton!
    
    var playerTurn: Bool = true
    var resultTable = [[5,5,5],[5,5,5],[5,5,5]]
    var lastTouch: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        but5.center = view.center
        but2.center.x = view.center.x
        but2.center.y = view.center.y - 110
        but8.center.x = view.center.x
        but8.center.y = view.center.y + 110
        
        but4.center.x = view.center.x - 110
        but4.center.y = view.center.y
        but6.center.x = view.center.x + 110
        but6.center.y = view.center.y
        
        but1.center.x = view.center.x - 110
        but1.center.y = view.center.y - 110
        
        but3.center.x = view.center.x + 110
        but3.center.y = view.center.y - 110
        
        but7.center.x = view.center.x - 110
        but7.center.y = view.center.y + 110
        
        but9.center.x = view.center.x + 110
        but9.center.y = view.center.y + 110
        
        
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(RandomColor) , userInfo: nil, repeats: true)
        
               but1.setTitle(nil, for: .normal)
               but2.setTitle(nil, for: .normal)
               but3.setTitle(nil, for: .normal)
               but4.setTitle(nil, for: .normal)
               but5.setTitle(nil, for: .normal)
               but6.setTitle(nil, for: .normal)
               but7.setTitle(nil, for: .normal)
               but8.setTitle(nil, for: .normal)
               but9.setTitle(nil, for: .normal)
        
        topText.textAlignment = .center
        topText.center.x = view.center.x
        topText.center.y = 100
        
        exitBut.center.x = view.center.x
        exitBut.center.y = view.center.x + 400
        exitBut.layer.cornerRadius = 15
        exitBut.layer.borderWidth = 2
        exitBut.layer.borderColor = UIColor.blue.cgColor
        
        but1.layer.cornerRadius = 15
        but1.layer.borderWidth = 2
        but1.layer.borderColor = UIColor.blue.cgColor
        
        but2.layer.cornerRadius = 15
        but2.layer.borderWidth = 2
        but2.layer.borderColor = UIColor.blue.cgColor
        
        but3.layer.cornerRadius = 15
        but3.layer.borderWidth = 2
        but3.layer.borderColor = UIColor.blue.cgColor
        
        but4.layer.cornerRadius = 15
        but4.layer.borderWidth = 2
        but4.layer.borderColor = UIColor.blue.cgColor
        
        but5.layer.cornerRadius = 15
        but5.layer.borderWidth = 2
        but5.layer.borderColor = UIColor.blue.cgColor
        
        but6.layer.cornerRadius = 15
        but6.layer.borderWidth = 2
        but6.layer.borderColor = UIColor.blue.cgColor
        
        but7.layer.cornerRadius = 15
        but7.layer.borderWidth = 2
        but7.layer.borderColor = UIColor.blue.cgColor
        
        but8.layer.cornerRadius = 15
        but8.layer.borderWidth = 2
        but8.layer.borderColor = UIColor.blue.cgColor
        
        but9.layer.cornerRadius = 15
        but9.layer.borderWidth = 2
        but9.layer.borderColor = UIColor.blue.cgColor
        
        RandomColor()
    }
    @objc public func RandomColor() {
      
          let red   = CGFloat((arc4random() % 256)) / 255.0
          let green = CGFloat((arc4random() % 256)) / 255.0
          let blue  = CGFloat((arc4random() % 256)) / 255.0
          let alpha = CGFloat(1.0)
    
      UIView.animate(withDuration: 4, delay: 0, options:[ .autoreverse, .allowUserInteraction], animations: {
              self.view.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)
          }, completion:nil)
      }
 
    
    
    
    @IBAction func universalClick(_ sender: UIButton) {
        if sender.currentTitle == nil && playerTurn == true {
             sender.setTitle("X", for: .normal)
             topText.text = "Turn Player 2"
             playerTurn = false
            
            if sender == but1 {
                resultTable[0][0] = 1
                lastTouch = "1"
            }
            if sender == but2 {
                resultTable[0][1] = 1
                lastTouch = "1"
            }
            if sender == but3 {
                resultTable[0][2] = 1
                lastTouch = "1"
            }
            if sender == but4 {
                resultTable[1][0] = 1
                lastTouch = "1"
            }
            if sender == but5 {
                resultTable[1][1] = 1
                lastTouch = "1"
            }
            if sender == but6 {
                resultTable[1][2] = 1
                lastTouch = "1"
            }
            if sender == but7 {
                resultTable[2][0] = 1
                lastTouch = "1"
            }
            if sender == but8 {
                resultTable[2][1] = 1
                lastTouch = "1"
            }
            if sender == but9 {
                resultTable[2][2] = 1
                lastTouch = "1"
            }
         }
         if sender.currentTitle == nil && playerTurn == false {
             sender.setTitle("0", for: .normal)
             topText.text = "Turn Player 1"
             playerTurn = true
            
            if sender == but1 {
                resultTable[0][0] = 0
                lastTouch = "2"
            }
            if sender == but2 {
                resultTable[0][1] = 0
                lastTouch = "2"
            }
            if sender == but3 {
                resultTable[0][2] = 0
                lastTouch = "2"
            }
            if sender == but4 {
                resultTable[1][0] = 0
                lastTouch = "2"
            }
            if sender == but5 {
                resultTable[1][1] = 0
                lastTouch = "2"
            }
            if sender == but6 {
                resultTable[1][2] = 0
                lastTouch = "2"
            }
            if sender == but7 {
                resultTable[2][0] = 0
                lastTouch = "2"
            }
            if sender == but8 {
                resultTable[2][1] = 0
                lastTouch = "2"
            }
            if sender == but9 {
                resultTable[2][2] = 0
                lastTouch = "2"
            }
         }
        
        winCondition()
        
    }
    func winCondition(){
        var win:Bool = false
        var countBut: Int = 0
        var orderH: Int = 10
        var orderV: Int = 10
        var orderD: Int = 10
        var orderD2: Int = 10
        var scoreH:Int = 0
        var scoreV:Int = 0
        var scoreD:Int = 0
        var scoreD2:Int = 0
        var verticalReverse:Int = 2
       
       
        for row in 0...2 {
                for collum in 0...2{
                    if (orderH == resultTable[row][collum]) && (resultTable[row][collum] != 5){
                        scoreH = scoreH + 1
                      }
                    orderH = resultTable[row][collum]
                    }
                for collum in 0...2{
                    if (orderV == resultTable[collum][row]) && (resultTable[collum][row] != 5){
                        scoreV = scoreV + 1
                    }
                    orderV = resultTable[collum][row]
                    }
                if (orderD == resultTable[row][row]) && (resultTable[row][row] != 5){
                                       scoreD = scoreD + 1
                                   }
                    orderD = resultTable[row][row]
                if (orderD2 == resultTable[row][verticalReverse]) && (resultTable[row][verticalReverse] != 5){
                    scoreD2 = scoreD2 + 1}
                    orderD2 = resultTable[row][verticalReverse]
                    verticalReverse = verticalReverse - 1
                if scoreH == 2 || scoreV == 2 || scoreD == 2 || scoreD2 == 2{
                           win = true
                           break}
                    else {
                            scoreH = 0;orderH = 5;scoreV = 0;orderV = 5
                    }
                for collum in 0...2{
                    if resultTable[row][collum] == 5 {
                        break
                    } else {
                            countBut = countBut + 1
                    }
                }
         }
    
        if win == false && countBut == 9 {
                win = true
                lastTouch = "Standoff"
        }
        if win == true {
            showWinCon(player: String(lastTouch))
        }
        
    }
    
     func nextGame(){
        but1.setTitle(nil, for: .normal)
        but2.setTitle(nil, for: .normal)
        but3.setTitle(nil, for: .normal)
        but4.setTitle(nil, for: .normal)
        but5.setTitle(nil, for: .normal)
        but6.setTitle(nil, for: .normal)
        but7.setTitle(nil, for: .normal)
        but8.setTitle(nil, for: .normal)
        but9.setTitle(nil, for: .normal)
        topText.text = "Turn 1 Player"
        playerTurn = true
        
        for row in 0...2 {
            for collum in 0...2{
                resultTable[row][collum] = 5
            }
        }
    }
    func showWinCon(player: String){
        let alert = UIAlertController(title: "Game over", message: "Win Player \(player)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: {
            action in self.nextGame()
        }
        ))
        self.present(alert, animated: true, completion: nil)
    }
}
